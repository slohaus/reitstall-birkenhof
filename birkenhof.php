<?php
// Inkludiere die Klasse.
include('php/reitstall-birkenhof.php');

// Header.
echo $reitstall_birkenhof->header( 'Birkenhof' , 'Traditionell geführt und gemeinsam verwurzelt.' );
?>


<div id="page">
    
    <?php
    // Navigation.
    echo $reitstall_birkenhof->navigation();
    ?>
    
    <div id="bd">
        
        <!-- Content start -->
        <h2><span>Birkenhof</span><a class="toggle-control" href="#"></a></h2>
        <div class="clear"></div>
        
        <div class="toggle-content">
            
            <div class="row">
                <h3>Traditionell geführt</h3>
                <div class="cols">
                    <div class="col_l">
                        <p>
                            Unsere Familie führt den Aussiedlerhof bereits seit seiner Erbauung durch Bernhard und Betty Fellmann im Jahre 1963. Damals diente er der Viehzucht und der Landwirtschaft, wobei besonders Opa Fellmann die Leidenschaft für Pferde und das Kutschfahren mit in die Familie brachte. Seit 1990 wurde der Hof mehr und mehr in einen Pferdepensionsbetrieb umstrukturiert, der bis heute von Bernd und Gabriele Fellmann geleitet wird.
                        </p>
                    </div>
                    <div class="col_r">
                        <p>
                            Durch die reitsportlichen Ambitionen der Kinder wurde die Anlage zunehmend modernisiert und vergrößert. Landwirtschaftliche Traditionen, Wissen aus vier Generationen und die jahrzehntelange Erfahrung mit Tieren verhelfen uns mit moderner Technik die Pferdepension nun schon seit über 20 Jahren erfolgreich zu führen. Heute beherbergt der Hof über 50 Pferde und Ponys und dient vielen Reitschülern als Ausbildungsstätte.
                        </p>
                    </div>
                </div>
            </div>
        
            <div class="row">
                <h3>Gemeinsam verwurzelt</h3>
                <div class="cols">
                    <div class="col_l">
                        <p>
                            Landwirt Bernd Fellmann leitet den Betrieb und sorgt für den betriebseigenen Futteranbau. Seine Frau Gabriele kümmert sich um die Belange der Einsteller sowie um sämtliche Verwaltungsangelegenheiten. Tatkräftig unterstützt werden die beiden von ihren Kindern Linda, Lukas und Prisca.
                        </p>
                    </div>
                    <div class="col_r">
                        <p><a href="#2" class="thumb_link"><img src="/img/verwurzelt_m.jpg" /></a></p>
                    </div>
                </div>
                <div class="cols">
                    <div class="col_l">
                        <p>
                            Gymnasiallehrerin Linda Fellmann leitet die Organisation der Reitschule, unterstützt von Prisca bei der Betreuung der Reitschüler und Reitschulpferde. Agraringenieur Lukas Fellmann  unterstützt bei den täglichen Betriebsabläufen und kümmert sich um die Aus- und Fortbildung der Pferde sowie um unsere kleine Pferdezucht. Die Ehepartner von Linda und Lukas stehen den beiden tatkräftig zur Seite.
                        </p>
                        <p>
                            Unsere Familie ist tief verwurzelt mit dem Hof, den wir auch in Zukunft gemeinsam weiterführen werden.
                        </p>
                    </div>
                    <div class="col_r">
                        <p><a href="#3" class="thumb_link"><img src="/img/hochzeit_unbenannt-007_m.jpg" /></a></p>
                        <p><a href="#4" class="thumb_link"><img src="/img/hochzeit_IMG_0143_m.jpg" /></a></p>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- Content ende -->
        
    </div>
    
</div>
    
<!-- Supersized start -->
<div class="rb-slide-control">
    <a href="#" id="rb-next-slide"></a>
    <a href="#" id="rb-prev-slide"></a>
</div>
<script>
    
    $(document).ready(function()
    {
        
        // Initialisiere Supersized.
        $.supersized({
            autoplay:false,
            slides: [
                {image : '/img/EingangBirkenhof2.jpg'},
                {image : '/img/verwurzelt.jpg'},
                {image : '/img/hochzeit_unbenannt-007.jpg'},
                {image : '/img/hochzeit_IMG_0143.jpg'}
            ]
        });
        
    });
    
</script>
<!-- Supersized ende -->

<?php
// Footer.
echo $reitstall_birkenhof->footer();
?>