<?php
/*
 * Bei Änderungen des CSS:
 * - Im Header styles.php statt der CSS-Datei verlinken.
 * - Stlyesheet entwickeln, ändern etc.
 * - Final das generierte CSS aus dieser Datei als CSS-Datei speichern und
 *   im Header verlinken.
 */   

// Inkludiere die Klasse.
include('../php/reitstall-birkenhof.php');

// Gib das CSS aus.
$minified = true;
echo $reitstall_birkenhof->styles($minified);
?>