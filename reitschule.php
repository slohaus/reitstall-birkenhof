<?php
// Inkludiere die Klasse.
include('php/reitstall-birkenhof.php');

// Header.
echo $reitstall_birkenhof->header( 'Reitschule' , 'Qualifizierter und individueller Unterricht seit 1997 durch erfahrene Ausbilder.' );
?>


<div id="page">
    
    <?php
    // Navigation.
    echo $reitstall_birkenhof->navigation();
    ?>
    
    <div id="bd">
        
        <!-- Content start -->
        <h2><span>Reitschule</span><a class="toggle-control" href="#"></a></h2>
        <div class="clear"></div>
        
        <div class="toggle-content">
            
            <div class="row">
                <h3>Spass und Sport an einem Ort</h3>
                <div class="cols">
                    <div class="col_l">
                        <p>Wir bieten Unterricht für unterschiedliche Alters- und Leistungsstufen: Kinderführstunde, Longen- und Einzelunterricht, Gruppenstunden und Reitstunden für Erwachsene sowie Kursangebote mit Ausritten, Springgymnastik und Sitzkorrektur an der Longe.</p>
                    </div>
                    <div class="col_r">
                        <p>Regelmäßig finden Lehrgänge zum Deutschen Reitabzeichen statt.</p>
                    </div>
                </div>
            </div>
        
            <div class="row">
                <h3>Unsere Vorteile</h3>
                <ul>
                    <li>Reitunterricht ohne Vereinsbindung für Kinder, Jugendliche und Erwachsene</li>
                    <li>Keine Massenabfertigung</li>
                    <li>Kleine Lerngruppen (3-5 Reiter)</li>
                    <li>Ausgeglichene Pferde durch täglichen Koppelgang</li>
                    <li>Gesunderhaltung unserer Pferde durch regelmäßige Hufschmied- und Tierarzttermine</li>
                    <li>Ständige Erweiterung und Verbesserung des Lehrgangsangebots</li>
                    <li>Möglichkeit der Hufeisen- und Reitabzeichenabnahme</li>
                    <li>Jahrelange Erfahrung in der Kinder- und Jugendausbildung</li>
                </ul>
            </div>
        
            <div class="row">
                <h3>Vielseitig ausgebildet</h3>
                <div class="cols">
                    <div class="col_l">
                        <p>Wir legen großen Wert auf die vielseitige Ausbildung junger Reiter. Ausritte, Dressurunterricht, Sitzkorrektur an der Longe sowie Springgymnastik gehören zu den selbstverständlichen Inhalten von Reitkursen.</p>
                        <p>Schon  im Alter von 16 Jahren setzte sich Linda Fellmann,  mit der didaktischen und methodischen Umsetzung von Reitunterricht auseinander. Selbst ritt sie auf Turnieren bis zur Klasse M. Ihr späteres Studium und Referendariat zur Gymnasiallehrerin gaben ihr zusätzlich Impulse, abwechslungsreiche Reitstunden sowie problemorientierte und nachhaltige Reitkurse zu gestalten. Bereits mit 21 Jahren durfte sie ihre Prüfung zum Trainer A - Leistungssport im Landgestüt Dillenburg ablegen, was sie mit Bestnoten in Reitlehre und Unterrichtserteilung tat. Mittlerweile konnte sie auch als Richteranwärterin Erfahrungen sammeln und ihr Auge weiter schulen.</p>
                        <p>Heute wird Linda tatkräftig von einem qualifizierten Team unterstützt. Der Betrieb ist stolz darauf, den Unterricht niveauvoll von ausschließlich geprüften Ausbildern in familiärer Atmosphäre durchführen zu können. Erfolgreiche Reitabzeichenprüfungen und Turnierstarts beweisen, dass bei uns Reitschüler von Beginn an mit der nötigen Sorgfalt gefördert werden. Bestes Beispiel ist hierfür Prisca Fellmann, die - wie schon ihre Geschwister Jahre zuvor - mittlerweile Mitglied im rheinhessischen Regionalkader ist.</p>
                    </div>
                    <div class="col_r">
                        <p><a href="#2" class="thumb_link"><img src="/img/Reitschule_1_m.jpg" /></a></p>
                        <p><a href="#3" class="thumb_link"><img src="/img/Reitschule_2_m.jpg" /></a></p>
                        <p><a href="#4" class="thumb_link"><img src="/img/Reitschule_3_m.jpg" /></a></p>
                        <p><a href="#5" class="thumb_link"><img src="/img/Reitschule_4_m.jpg" /></a></p>
                    </div>
                </div>
                
            </div>
            
        </div>
        <!-- Content ende -->
        
    </div>
    
</div>
    
<!-- Supersized start -->
<div class="rb-slide-control">
    <a href="#" id="rb-next-slide"></a>
    <a href="#" id="rb-prev-slide"></a>
</div>
<script>
    
    $(document).ready(function()
    {
        
        // Initialisiere Supersized.
        $.supersized({
            autoplay:false,
            slides: [
                {image : '/img/Reitschule_Hintergrund.jpg'},
                {image : '/img/Reitschule_1.jpg'},
                {image : '/img/Reitschule_2.jpg'},
                {image : '/img/Reitschule_3.jpg'},
                {image : '/img/Reitschule_4.jpg'}
            ]
        });
        
    });
    
</script>
<!-- Supersized ende -->

<?php
// Footer.
echo $reitstall_birkenhof->footer();
?>