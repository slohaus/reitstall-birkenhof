<?php
// Inkludiere die Klasse.
include('php/reitstall-birkenhof.php');

// Header.
echo $reitstall_birkenhof->header( 'Kontakt' , '' );
?>


<div id="page">
    
    <?php
    // Navigation.
    echo $reitstall_birkenhof->navigation();
    ?>
    
    <div id="bd">
        
        <!-- Content start -->
        <h2><span>Kontakt</span><a class="toggle-control" href="#"></a></h2>
        <div class="clear"></div>
        
        <div class="toggle-content">
            
            <div class="row">
                <h3>Anschrift</h3>
                <div class="cols">
                    <div class="col_l">
                        <p>
                            Reitstall Birkenhof<br>
                            Birkenhof<br>
                            67550 Worms-Rheindürkheim
                        </p>
                        <p>
                            Telefon: 06242 6111<br>
                            E-Mail: <a href="mailto:info@reitstallbirkenhof.de">info@reitstallbirkenhof.de</a>
                        </p>
                    </div>
                    <div class="col_r">
                        <p>
                            Sie interessieren sich für Reitunterricht oder möchten eine Box anmieten? Dann schicken Sie uns einfach eine E-Mail unter Angabe Ihrer Kontaktdaten und Ihres Anliegens.
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <h3>Anfahrt</h3>
                <iframe width="710" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.de/maps?f=q&amp;source=s_q&amp;hl=de&amp;geocode=&amp;q=76756,+Bellheim&amp;aq=&amp;sll=49.195054,8.293991&amp;sspn=0.047788,0.077076&amp;ie=UTF8&amp;hq=&amp;hnear=Bellheim,+Rheinland-Pfalz&amp;ll=49.193552,8.288535&amp;spn=0.023895,0.038538&amp;t=m&amp;z=14&amp;output=embed"></iframe>
            </div>
            
        </div>
        <!-- Content ende -->
        
    </div>
    
</div>
    
<!-- Supersized start -->
<div class="rb-slide-control">
    <a href="#" id="rb-next-slide"></a>
    <a href="#" id="rb-prev-slide"></a>
</div>
<script>
    
    $(document).ready(function()
    {
        
        // Initialisiere Supersized.
        $.supersized({
            autoplay:false,
            slides: [
                {image : '/img/auge_IMG_2783.jpg'}
            ]
        });
        
    });
    
</script>
<!-- Supersized ende -->

<?php
// Footer.
echo $reitstall_birkenhof->footer();
?>