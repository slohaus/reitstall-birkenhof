<?php
// Inkludiere die Klasse.
include('php/reitstall-birkenhof.php');

// Header.
echo $reitstall_birkenhof->header( 'Pferdepension' , 'In familiärem Ambiente fühlen sich alle Sparten der Reiterei wohl.' );
?>


<div id="page">
    
    <?php
    // Navigation.
    echo $reitstall_birkenhof->navigation();
    ?>
    
    <div id="bd">
        
        <!-- Content start -->
        <h2><span>Pferdepension</span><a class="toggle-control" href="#"></a></h2>
        <div class="clear"></div>
        
        <div class="toggle-content">
            
            <div class="row">
                <h3>Willkommen daheim</h3>
                <div class="cols">
                    <div class="col_l">
                        <p>
                            Eine Pferdepension, die seit über 20 Jahren wächst. Generationen, die ein Leben lang mit Pferden verwurzelt sind. Wissen und Gespür, das von Generation zu Generation weitergegeben wurde.
                        </p>
                    </div>
                    <div class="col_r">
                        <p>
                            Wir verstehen uns als Pferdepension mit Tradition in familiärem Ambiente. Unsere Großfamilie wohnt direkt an der Reitanlage, wodurch ständige Kontrolle der Pferde gewährleistet ist.
                        </p>
                    </div>
                </div>
            </div>
        
            <div class="row">
                <h3>Unsere Leistungen</h3>
                <ul>
                    <li>Vollpension (in Innen- und Außenboxen sowie Paddockboxen)</li>
                    <li>Eigener Futteranbau</li>
                    <li>Beritt und Verkauf auf Anfrage durch Springreiter Lukas Fellmann</li>
                    <li>Reithalle, Reitplatz, Longierplatz, kleiner Springgarten</li>
                    <li>Einzel- und Gruppenpaddocks</li>
                    <li>Sommerweiden</li>
                    <li>Tolles Ausreitgelände in den angrenzenden Rheinauen und Feldern</li>
                    <li>Zwei Pferdewaschplätze</li>
                    <li>Überdachter Beschlagplatz</li>
                    <li>Hängerstellplätze</li>
                    <li>Wurmkur-Management</li>
                    <li>Reitunterricht durch die betriebseigene Reitschule</li>
                </ul>
            </div>
        
            <div class="row">
                <div class="cols">
                    <div class="col_l">
                        <p><a href="#2" class="thumb_link"><img src="/img/pferdepension_IMG_1771_m.jpg" /></a></p>
                    </div>
                    <div class="col_r">
                        <h4>Vollpension</h4>
                        <p>Unsere Paddockboxen mit weitläufigem Ausblick.</p>
                    </div>
                </div>
                
                <div class="cols">
                    <div class="col_l">
                        <p><a href="#3" class="thumb_link"><img src="/img/futteranbau_IMG_1986_m.jpg" /></a></p>
                        <p><a href="#4" class="thumb_link"><img src="/img/eigener_Futteranbau_m.jpg" /></a></p>
                    </div>
                    <div class="col_r">
                        <h4>Eigener Futteranbau</h4>
                        <p>Futter ist bei uns Chefsache! Gutes Futter ist der Grundstock einer Pferdepension. Wir garantieren Ihnen und Ihrem Pferd beste Qualität. Futter aus eigenem nachhaltigen Anbau und sorgfältiger Herstellung auf gepflegten Flächen ist bei uns eine Selbstverständlichkeit. Hafer und Gerste dienen als Kraftfutter, Gras und Luzerne wird zur Heulage- und Heugewinnung angebaut. Wir haben den Anspruch, unser Futter ständig zu kontrollieren und nur im besten Zustand zu verfüttern. Die Kombination aus langjähriger Erfahrung im Futteranbau und Liebe zur Landwirtschaft führen zu einer unschlagbaren Futterqualität.</p>
                    </div>
                </div>
                
                <div class="cols">
                    <div class="col_l">
                        <p><a href="#5" class="thumb_link"><img src="/img/lukas_DSC_0132_m.jpg" /></a></p>
                    </div>
                    <div class="col_r">
                        <h4>Beritt und Verkauf auf Anfrage durch Springreiter Lukas Fellmann</h4>
                        <p>Lukas genoss eine fundierte Dressurausbildung und ritt hier bis zur Klasse M. Seine Leidenschaft galt aber schon immer dem Springreiten, wobei ihm seine Dressurausbildung für ein stilistisch sauberes Parcoursreiten zugutekommt. Auch das Vielseitigkeitsreiten praktizierte er einige Jahre. Sein Praxissemester absolvierte er im Spring- und Ausbildungsstall Oliver Ross, was besonders die Ausbildung seiner jungen Pferde prägte.  Mittlerweile kann Lukas zahlreiche Siege und Platzierungen bis in die schwerste Klasse und in Jungpferdeprüfungen nachweisen. Als Nächstes wird der Agraringenieur mit dem Schwerpunkt Pferdewissenschaften die Prüfung zum Pferdewirtschaftsmeister absolvieren.</p>
                    </div>
                </div>
                
                <div class="cols">
                    <div class="col_l">
                        <p><a href="#6" class="thumb_link"><img src="/img/reithalle_IMG_3033_m.jpg" /></a></p>
                    </div>
                    <div class="col_r">
                        <h4>Reithalle</h4>
                        <p>Unsere Reithalle ist ausgestattet mit einem Flies-Sand-Gemisch.</p>
                    </div>
                </div>
                
                <div class="cols">
                    <div class="col_l">
                        <p><a href="#7" class="thumb_link"><img src="/img/reitplatz_IMG_2546_m.jpg" /></a></p>
                    </div>
                    <div class="col_r">
                        <h4>Reitplatz</h4>
                        <p>Unser Reitplatz ist ausgestattet mit Spezialsand der Firma Heuss und bietet besten Reitkomfort für jedes Pferd und jede Reitweise.</p>
                    </div>
                </div>
                
                <div class="cols">
                    <div class="col_l">
                        <p><a href="#8" class="thumb_link"><img src="/img/longierplatz_IMG_2542_m.jpg" /></a></p>
                    </div>
                    <div class="col_r">
                        <h4>Longierplatz</h4>
                        <p>Unser Longierplatz wurde 2007 neu angelegt. Gefüllt ist er ebenso wie die Reithalle mit einem Flies-Sand-Gemisch.</p>
                    </div>
                </div>
                
                <div class="cols">
                    <div class="col_l">
                        <p><a href="#9" class="thumb_link"><img src="/img/sommerweiden_IMG_3150_m.jpg" /></a></p>
                    </div>
                    <div class="col_r">
                        <h4>Sommerweiden</h4>
                        <p>Die Sommerweiden sind in den Monaten April bis Oktober zusätzlich nutzbar - natürlich im Pensionspreis enthalten.</p>
                    </div>
                </div>
                
                <div class="cols">
                    <div class="col_l">
                        <p><a href="#10" class="thumb_link"><img src="/img/ausritt_IMG_1955_m.jpg" /></a></p>
                    </div>
                    <div class="col_r">
                        <h4>Tolles Ausreitgelände in den angrenzenden Rheinauen und Feldern</h4>
                    </div>
                </div>
                
            </div>
            
        </div>
        <!-- Content ende -->
        
    </div>
    
</div>
    
<!-- Supersized start -->
<div class="rb-slide-control">
    <a href="#" id="rb-next-slide"></a>
    <a href="#" id="rb-prev-slide"></a>
</div>
<script>
    
    $(document).ready(function()
    {
        
        // Initialisiere Supersized.
        $.supersized({
            autoplay:false,
            slides: [
                {image : '/img/kutsche_IMG_2680.jpg'},
                {image : '/img/pferdepension_IMG_1771.jpg'},
                {image : '/img/futteranbau_IMG_1986.jpg'},
                {image : '/img/eigener_Futteranbau.jpg'},
                {image : '/img/lukas_DSC_0132.jpg'},
                {image : '/img/reithalle_IMG_3033.jpg'},
                {image : '/img/reitplatz_IMG_2546.jpg'},
                {image : '/img/longierplatz_IMG_2542.jpg'},
                {image : '/img/sommerweiden_IMG_3150.jpg'},
                {image : '/img/ausritt_IMG_1955.jpg'}
            ]
        });
        
    });
    
</script>
<!-- Supersized ende -->

<?php
// Footer.
echo $reitstall_birkenhof->footer();
?>