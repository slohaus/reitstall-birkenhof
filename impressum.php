<?php
// Inkludiere die Klasse.
include('php/reitstall-birkenhof.php');

// Header.
echo $reitstall_birkenhof->header( 'Impressum' , '' );
?>


<div id="page">
    
    <?php
    // Navigation.
    echo $reitstall_birkenhof->navigation();
    ?>
    
    <div id="bd">
        
        <!-- Content start -->
        <h2><span>Impressum</span><a class="toggle-control" href="#"></a></h2>
        <div class="clear"></div>
        
        <div class="toggle-content">
            
            <div class="row">
                <h3>Anschrift</h3>
                <p>
                    Reitstall Birkenhof
                </p>
                <p>
                    Inh. Bernd Fellmann<br>
                    Birkenhof<br>
                    67550 Worms Rheindürkheim
                </p>
                <p>
                    Telefon: 06242 6111<br>
                    E-Mail: <a href="mailto:info@reitstallbirkenhof.de">info@reitstallbirkenhof.de</a>
                </p>
            </div>
            
            <div class="row">
                <h3>Haftungsausschluss</h3>
                <h4>Inhalte</h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate
                </p>
                
                <h4>Links</h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate
                </p>
                
                <h4>Urheberrecht</h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate
                </p>
                
                <h4>Datenschutz</h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate
                </p>
            </div>
            
            <div class="row">
                <h3>Google Analytics</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate
                </p>
            </div>
            
        </div>
        <!-- Content ende -->
        
    </div>
    
</div>
    
<!-- Supersized start -->
<div class="rb-slide-control">
    <a href="#" id="rb-next-slide"></a>
    <a href="#" id="rb-prev-slide"></a>
</div>
<script>
    
    $(document).ready(function()
    {
        
        // Initialisiere Supersized.
        $.supersized({
            autoplay:false,
            slides: [
                {image : '/img/Impressum_Hintergrund.jpg'}
            ]
        });
        
    });
    
</script>
<!-- Supersized ende -->

<?php
// Footer.
echo $reitstall_birkenhof->footer();
?>