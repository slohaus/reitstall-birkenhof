/*
 * reitstall-birkenhof.js
 *
 * Alle benötigten JavaScript-Funktionen.
 */


/* ---------------------
 * Custom für supersized (Callback-Funktionen)
 * Docs: http://stackoverflow.com/a/6906380/
 * --------------------- */
(function($){
	
	var teaser = false;
	
	theme = {
	 	
	 	_init : function(){
			
			// Wenn da mindestens 2 Slides sind.
			if(api.options.slides.length > 1)
			{
				// Zeig die Slide-Controls.
				$('.rb-slide-control').show();
        
				// Setz die Listener und Handler für die Slide-Controls.
				$('#rb-next-slide').click(function()
				{
				    api.nextSlide();
				    return false;
				});
				$('#rb-prev-slide').click(function()
				{
				    api.prevSlide();
				    return false;
				});
			}
			
			// Wenn da Teaser sind.
			if( $('.teaser').length > 0 )
			{
				teaser = true;
				reitstall_birkenhof.show_teaser(vars.current_slide);
			}
			
		},
	 	afterAnimation : function(){
			
			// Wechsle den Teaser.
			if(teaser)
			{
				reitstall_birkenhof.show_teaser(vars.current_slide);
			}
	 	}
	 
	};
	
})(jQuery);


/* ---------------------
 * Sonstige Funktionen
 * --------------------- */
var reitstall_birkenhof = {
	
	// Listener / Handler für das Content-"Akordeon".
	content_akkordeon : function()
	{
                $('.toggle-control').click(function()
                {
			var h2 = $(this).parent('h2');
			if(h2.hasClass('closed'))
			{
				
				h2.animate({width:720},function(){h2.toggleClass('closed');});
			} else
			{
				h2.toggleClass('closed');
				h2.animate({width:15});
			}
			$('.toggle-content').slideToggle();
			return false;
                });
	},
	
	// Zeige den aktuelle Teaser.
	show_teaser : function(target)
	{
		target++;
		target_class = '.teaser-' + target;
		$('.teaser.current')
			.fadeOut()
			.removeClass('current'); // Versteck den alten.
		$(target_class) // Zeig den akuellen
			.fadeIn()
			.addClass('current');
		
	},
	
	// Thumb-Links: Ändern das Hintergrund-Bild.
	thumb_links : function()
	{
		$('a.thumb_link').click(function()
		{
			target = $(this).attr('href').replace('#', '');
			api.goTo(target);
			return false;
		});
	}
	
}