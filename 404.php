<?php
// Inkludiere die Klasse.
include('php/reitstall-birkenhof.php');

// Header.
echo $reitstall_birkenhof->header( '404' , '' );
?>


<div id="page">
    
    <?php
    // Navigation.
    echo $reitstall_birkenhof->navigation();
    ?>
    
    <div id="bd">
        
        <!-- Content start -->
        <h2><span>Ups, hier fehlt wohl etwas...</span><a class="toggle-control" href="#"></a></h2>
        <div class="clear"></div>
        
        <div class="toggle-content">
            
            <div class="row">
                <p>Hier geht´s zurück zur <a href="/">Startseite</a></p>
            </div>
            
        </div>
        <!-- Content ende -->
        
    </div>
    
</div>
    
<!-- Supersized start -->
<div class="rb-slide-control">
    <a href="#" id="rb-next-slide"></a>
    <a href="#" id="rb-prev-slide"></a>
</div>
<script>
    
    $(document).ready(function()
    {
        
        // Initialisiere Supersized.
        $.supersized({
            autoplay:false,
            slides: [
                {image : '/img/auge_IMG_2783.jpg'}
            ]
        });
        
    });
    
</script>
<!-- Supersized ende -->

<?php
// Footer.
echo $reitstall_birkenhof->footer();
?>