<?php
/** 
 * Class reitstall_birkenhof
 *
 * Benötigte Methoden für die ganze Seite.
 */

class reitstall_birkenhof
{
    
    
    /**
     * Function styles
     *
     * Gibt die CSS-Styles (basierend auf den less-Dateien) aus.
     * Lessphp docs: http://leafo.net/lessphp/docs/
     */
    function styles( $compressed = false )
    {
        header("Content-Type: text/css"); // Setz den Header auf CSS.
        require "../php/lessphp/lessc.inc.php";                        // Inkludiere die lessphp-Klasse.
        $less = new lessc;
        if($compressed) { $less->setFormatter("compressed"); }  // Komprimiere ggf.
        return $less->compileFile("../less/master.less");          // Gib das CSS aus.
    }
    
    
    /**
     * Function header
     *
     * Gibt den HTML-Header inkl. aller Metadaten aus.
     */
    function header( $title = '' , $description = '' )
    {
        // Bau den Titel.
        $tb = 'Reitstall Birkenhof'; // Titel Basis.
        $t = ($title != '') ? $title . ' | ' . $tb : $tb; // Finaler Titel.
        
        // Setz die description.
        $d = ($description != '') ? $description : $tb; // Wenn es keine gibt, nimm den title base.
        
        // Ermittle die aktuelle URL (für das Facebook-Meta).
        $u = "http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
        
        // Setz den Bildpfad (für das Facebook-Meta).
        $i = "http://".$_SERVER["HTTP_HOST"].'/img/LogoReitstallBirkenhof.png';
    ?>
        
        <!DOCTYPE html>
    
        <!-- Recommended by Paul Irish: http://paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
        <!--[if lt IE 7]> <html class="ie lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
        <!--[if IE 7]>    <html class="ie lt-ie9 lt-ie8"> <![endif]-->
        <!--[if IE 8]>    <html class="ie lt-ie9"> <![endif]-->
        <!--[if gt IE 8]><!--> <html class=""> <!--<![endif]-->
        
        <head>
            <meta charset="utf-8">
            <title><?php echo $t; ?></title>
            <meta name="description" content="<?php echo $d; ?>">
        
            <!-- Styles. -->
            <link type="text/css" rel="stylesheet" href="http://fast.fonts.com/cssapi/c42ac6a7-b14e-4424-afcc-d3863e4c4f45.css"/>
            <link type="text/css" rel="stylesheet" href="/css/styles.2.0.css"/>
            <link media="print" type="text/css" rel="stylesheet" href="/css/print.css"/>
    
            <!-- JavaScript -->
            <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
            <script type="text/javascript" src="js/supersized/slideshow/js/supersized.3.2.7.min.js"></script>
            <script type="text/javascript" src="js/reitstall-birkenhof.js"></script>
    
            <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
            <!--[if lt IE 9]>
              <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->
            
            <!-- Favicon -->
            <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
            <link rel="icon" href="/img/favicon.ico" type="image/x-icon">
                
            <!-- Facebook Meta -->
            <meta property="og:url" content="<?php echo $u; ?>" />
            <meta property="og:title" content="<?php echo $t; ?>" />
            <meta property="og:description" content="<?php echo $d; ?>" />
            <meta property="og:image" content="<?php echo $i; ?>" /> 
        
        </head>
        <body>
    
    <?php  
    }
    
    
    /**
     * Function navigation
     *
     * Gibt die Navigation aus.
     */
    function navigation()
    {
    ?>  
    
        <!-- Header (Navigation) start -->
        <div id="hd">
            <h1><a href="/"><span>Reitstall Birkenhof</span></a></h1>
            <ul id="navigation">
                <li><?php echo $this->link_builder('birkenhof','Birkenhof'); ?></li>
                <li><?php echo $this->link_builder('pferdepension','Pferdepension'); ?></li>
                <li><?php echo $this->link_builder('reitschule','Reitschule'); ?></li>
                <li><?php echo $this->link_builder('kontakt','Kontakt'); ?></li>
                <li class="last-child"><?php echo $this->link_builder('impressum','Impressum'); ?></li>
            </ul>
        </div>
        <!-- Header (Navigation) ende -->
    
    <?php
    }
    
    
    /**
     * Function footer
     *
     * Gibt den Footer aus.
     */
    function footer( $storer = false )
    {
        if ($storer)
        {
            $img = '/img/stoerer-50jahre.png';
            $height = '120px';
        } else
        {
            $img = '/img/illu-footer.png';
            $height = '50px';
        }
    ?>
        
        <!-- Footer start -->
        <div id="ft"><div class="inner" style="height:<?php echo $height; ?>;"><img src="<?php echo $img; ?>" /></div></div>
        <!-- Footer ende -->
        
        <!-- (Allgemeines) JavaScript start -->
        <script>
            
            $(document).ready(function()
            {
                
                reitstall_birkenhof.content_akkordeon();
                reitstall_birkenhof.thumb_links();
               
            });
            
        </script>
        <!-- (Allgemeines) JavaScript ende -->
        
        </body>
        </html>
        
    <?php   
    }
    
    
    /**
     * Function link_builder
     *
     * Generiert das <a>-Tag mit eventueller "current"-Klasse
     */
    function link_builder( $target , $name )
    {
        
        $link = '';
        $link .= '<a href="';
        $link .= $target;
        $link .= '"';
        
        if (strpos($_SERVER["REQUEST_URI"],$target))
        {
            $link .= ' class="current"';
        }
        
        $link .= '>';
        $link .= $name;
        $link .= '</a>';
        
        return $link;
    }
    
}


// Instanzieren.
$reitstall_birkenhof = new reitstall_birkenhof();