<?php
// Inkludiere die Klasse.
include('php/reitstall-birkenhof.php');

// Header.
echo $reitstall_birkenhof->header( 'Willkommen' , 'Spaß und Sport an einem Ort.' );
?>


<div id="page">
    
    <?php
    // Navigation.
    echo $reitstall_birkenhof->navigation();
    ?>
    
    <div id="bd">
        
        <!-- Teaser start -->
        <div class="teaser teaser-1" style="top: 70%; width: 800px;">
            <div class="inner">
                <h2>Birkenhof</h2>
                <p>Erfahren Sie mehr über den Birkenhof und unsere Familie.</p>
                <p><a class="button" href="/birkenhof">&raquo; Zum Birkenhof</a></p>
            </div>
        </div>
        <div class="teaser teaser-2" style="top: 60%; width: 700px;">
            <div class="inner">
                <h2>Pferdepension</h2>
                <p>In familiärem Ambiente fühlen sich alle Sparten der Reiterei wohl.</p>
                <p><a class="button" href="/pferdepension">&raquo; Zur Pferdepension</a></p>
            </div>
        </div>
        <div class="teaser teaser-3" style="top: 58%; width: 400px;">
            <div class="inner">
                <h2>Reitschule</h2>
                <p>Qualifizierter und individueller Unterricht seit 1997 durch erfahrene Ausbilder.</p>
                <p><a class="button" href="/reitschule">&raquo; Zur Reitschule</a></p>
            </div>
        </div>
        <!-- Teaser ende -->
        
    </div>
    
</div>
    
<!-- Supersized start -->
<div class="rb-slide-control">
    <a href="#" id="rb-next-slide"></a>
    <a href="#" id="rb-prev-slide"></a>
</div>
<script>
    
    $(document).ready(function()
    {
        
        // Initialisiere Supersized.
        $.supersized({
            autoplay:true,
            slides: [
                {image : '/img/EingangBirkenhof2.jpg'},
                {image : '/img/kutsche_IMG_2680.jpg'},
                {image : '/img/Reitschule_Hintergrund.jpg'}
            ]
        });
        
    });
    
</script>
<!-- Supersized ende -->

<?php
// Footer.
echo $reitstall_birkenhof->footer(true);
?>